# Iot mqtt

IOT tp project with mqtt to read data, send to server and send to influxDb.

## Requirements

For development, you will only need Node.js and influxDb.

### Node
- #### Node installation on Windows

  Just go on [official Node.js website](https://nodejs.org/) and download the installer.
Also, be sure to have `git` available in your PATH, `npm` might need it (You can find git [here](https://git-scm.com/)).

- #### Node installation on Ubuntu

  You can install nodejs and npm easily with apt install, just run the following commands.

      $ sudo apt install nodejs
      $ sudo apt install npm

### InfluxDb
[InfluxDb](https://github.com/influxdata/influxdb/blob/master/README.md)

### Npm installation
  After installing node run the following command in the project.
      $ npm install -g 
      $ npm install  

### Run
./run.sh