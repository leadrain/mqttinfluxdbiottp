const http = require('http');
var mqtt = require('mqtt');
var client = mqtt.connect('mqtt://localhost');

var donnee;

client.on('connect', function () { 
    console.log("Passerelle ready");
})

setInterval(httpRequest, 30000);

function httpRequest() {
    http.get('http://192.168.20.104/api/xdevices.json?cmd=10', (resp) => {
        let data = '';

        resp.on('data', (chunk) => {
            data += chunk;
        });

        resp.on('end', () => {
            donnee = JSON.parse(data)
            mqttServe()
        });

    }).on("error", (err) => {
        console.log("Error: " + err.message);
    });
}

function mqttServe() {
    client.publish('topic', JSON.stringify(donnee));
    console.log('Données envoyées dans le serveur');
}

