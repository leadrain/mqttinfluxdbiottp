var mosca = require('mosca');
var mqtt = require('mqtt')
var client = mqtt.connect('mqtt://localhost')
const os = require('os')
const Influx = require('influx');

const influx = new Influx.InfluxDB({
	host: '134.214.119.235',
	database: 'mydb',
	port: 8086,
	schema: [
		{
			measurement: 'ecodata',
			fields: {
				Nom_Produit: Influx.FieldType.STRING,
				Tarif_En_Cours: Influx.FieldType.STRING,
				Watts: Influx.FieldType.INTEGER,
				//T1_BASE: Influx.FieldType.INTEGER,
				Index_C1: Influx.FieldType.INTEGER,
				Index_C2: Influx.FieldType.INTEGER,
			},
			tags: [
				'host'
			],
			timestamp: Influx.FieldType.timestamp

		}
	]
});

var settings = {
	port: 1883
}

var server = new mosca.Server(settings);

server.on('ready', function () {
	console.log("Serveur ready");
});

client.on('connect', function () {
	client.subscribe('topic')
})

client.on('message', function (topic, dataReveive) {
	data = JSON.parse(dataReveive);
	console.log("Données reçues de la passerelle")
	writeData()
})

function writeData() {
	influx.writePoints([
		{
			measurement: 'ecodata',
			tags: { host: os.hostname() },
			fields: {
				Nom_Produit: data.product,
				Tarif_En_Cours: data.T1_PTEC,
				Watts: data.T1_PAPP,
				//Base: data.T1_BASE,
				Index_C1: data.INDEX_C1,
				Index_C2: data.INDEX_C2
			},
			timestamp: 1571472118		
		}
	]).catch(err => {
		console.error(`Error saving data to InfluxDB! ${err.stack}`)
	}).then(
		console.log("Données envoyées dans bdd")
	)
};

