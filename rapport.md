# TP IOT 2

*Romain LE DUC*
*Steevin AMOUZOU*
*Valentin SALMERON*

### Présentation de votre travail
Ce rapport présente la conception et la mise en oeuvre d'une application client-serveur pour récupérer les données de consommation d'un compteur EDF.
### Justification des choix techniques retenus

#### Mqtt - NodeJs

Nous avons fait le choix d'utiliser la technologie de transfert [MQTT](http://mqtt.org/). La raison de ce choix est que ce protocole est moins verbeux que le HTTP. De plus, il est basé sur le protocole TCP/IP, ce qui signifie qu'il est très flexible. Enfin, MQTT introduit la notion de qualité de service (QOS) qui permet à un client de s’assurer qu’un message a bien été transmis, avec différents niveaux de fiabilité.
 
Pour implémenter cette technologies, nous avons fait le choix d'utiliser [Javascript](https://fr.wikipedia.org/wiki/JavaScript) que nous maitrisons et en mettant en place un serveur [Nodejs](https://nodejs.org/fr/).

#### Influx DB
Au départ, nous nous étions dirigés vers MongoDB, une base de données noSQL orientée documents. Nous avions opté pour ce SGBD car celui-ci utilise du JSON, format nativement pris en charge par Javascript. Malheureusement, une contrainte technique nous a empêché d'utiliser MongoDB.
En effet, Grafana ne possède pas de plugin pour prendre en charge les bases de données MongoDB.
Ainsi, nous avons dû nous réorienter vers [InfluxDB](https://www.influxdata.com/get-influxdb/). Cette réorientation ne nous a pas perturbé puisque InfluxDB est très simple d'utilisation pour l'usage que nous allions en faire.
De plus, InfluxDB ajoute automatiquement les dates d'insertion des données que l'on insert en base, ce qui nous a beaucoup aidé.

#### Grafana
Nous avons choisi d'utiliser [Grafana](https://grafana.com/) pour monitorer les données du compteur parce que cet outil se branche facilement avec InfluxDB. Grafana permet aussi de créer des tableaux de bord pouvant contenir de nombreuses manières de visualiser nos données (diagrammes en barre, jauge, métrique, ...).


### Mise en oeuvre des technologies retenues

#### Passerelle 

Pour commencer, nous avons importé les packages HTTP et MQTT dans notre passerelle .

```javascript=
const http = require('http');
var mqtt = require('mqtt');
```

Ensuite, on crée un client et on le connecte à notre serveur en local.
```javascript=
var client = mqtt.connect('mqtt://localhost');

client.on('connect', function () { 
    console.log("Passerelle ready");
})
```
Puis, on utilise une fonction pour avoir un intervalle de 30 secondes avant de requêter le tableau électrique.

```javascript=
setInterval(httpRequest, 30000);

function httpRequest() {
    http.get('http://192.168.20.104/api/xdevices.json?cmd=10', (resp) => {
        let data = '';

        resp.on('data', (chunk) => {
            data += chunk;
        });

        resp.on('end', () => {
            donnee = JSON.parse(data)
            mqttServe()
        });

    }).on("error", (err) => {
        console.log("Error: " + err.message);
    });
}
```
Enfin, on envoie les données sur le serveur.

```javascript=
function mqttServe() {
    client.publish('topic', JSON.stringify(donnee));
    console.log('Données envoyées dans le serveur');
}
```

Voici le résultat que nous obtonons dans la console du programme : 

```javascript=
Données reçues de la passerelle
{ product: 'Eco-devices',
  T1_PTEC: 'TH..',
  T1_PAPP: 120,
  T1_BASE: 1875472,
  T2_PTEC: '----',
  T2_PAPP: 0,
  T2_BASE: 0,
  INDEX_C1: 784513,
  INDEX_C2: 10861 }
Données envoyées dans bdd
```

#### Serveur

Pour commencer, nous avons importé les packages importants : mosca pour le serveur et MQTT. Nous avons aussi importé os et influx pour la base de donnée.

```javascript=
var mosca = require('mosca');
var mqtt = require('mqtt')
var client = mqtt.connect('mqtt://localhost')
const os = require('os')
const Influx = require('influx');
```

Ensuite, nous avons instancié les paramètres pour la connexion à InfluxDB.
```javascript=
const influx = new Influx.InfluxDB({
	host: '134.214.119.235',
	database: 'mydb',
	port: 8086,
	schema: [
		{
			measurement: 'ecodata',
			fields: {
				Nom_Produit: Influx.FieldType.STRING,
				Tarif_En_Cours: Influx.FieldType.STRING,
				Watts: Influx.FieldType.INTEGER,
				//T1_BASE: Influx.FieldType.INTEGER,
				Index_C1: Influx.FieldType.INTEGER,
				Index_C2: Influx.FieldType.INTEGER
			},
			tags: [
				'host'
			]
		}
	]
});
```

Puis nous démarrons le serveur MQTT.

```javascript=
var settings = {
	port: 1883
}
var server = new mosca.Server(settings);
server.on('ready', function () {
	console.log("Serveur ready");
});
```

Ensuite, nous établissons la connexion entre le cient et le serveur MQTT. Nous subscribons topic afin de récupérer les données envoyées de la passerelle. Puis nous récupérons les données que nous ajoutons à data.
```javascript=
client.on('connect', function () {
	client.subscribe('topic')
})

client.on('message', function (topic, dataReveive) {
	data = JSON.parse(dataReveive);
	console.log("Données reçues de la passerelle")
	writeData()
})
```

Pour finir, nous envoyons dans InfluxDB les données reçues de la passerelle pour enrichir la mesure 'ecodata'.

```javascript=
function writeData() {
	influx.writePoints([
		{
			measurement: 'ecodata',
			tags: { host: os.hostname() },
			fields: {
				Nom_Produit: data.product,
				Tarif_En_Cours: data.T1_PTEC,
				Watts: data.T1_PAPP,
				//Base: data.T1_BASE,
				Index_C1: data.INDEX_C1,
				Index_C2: data.INDEX_C2
			},
		}
	]).catch(err => {
		console.error(`Error saving data to InfluxDB! ${err.stack}`)
	}).then(
		console.log("Données envoyées dans bdd")
	)
};
```


#### InfluxDb
Pour InfluxDB, on a installé la version 2.0.0. Ensuite, pour lancer le serveur Influx, on utilise la commande ```influxd```. Une fois, le serveur lancé, il se met en attente de requêtes clients qui doivent provenir du client MQTT et de Grafana. Le port d'écoute est le port 8086.

![Serveur InfluxDB](https://i.imgur.com/4xtapGv.png)

On a créé le schéma myDB, qui est notre schéma de référence pour recevoir les données provenant du compteur. Le *measurement* de référence est EcoData. Pour vérifier le bon fonctionnement de notre base, il suffit d'effectuer un simple requête SQL. L'envoi de requêtes depuis le client MQTT est expliqué juste au-dessus dans la partie Serveur.
Pour effectuer une requête SQL sur InfluxDB, on se sert de la console avec la commande ```influx```.
```sql 
select * from ecodata
```
On reçoit bien les données des sous-compteurs avec les informations sur le nom du produit, le tarif en cours, les watts ,l'hôte ainsi qu'un timestamp généré pour identifier la ligne.
```shell=
name: ecodata
time                Index_C1 Index_C2 Nom_Produit Tarif_En_Cours Watts host
----                -------- -------- ----------- -------------- ----- ----
1571065549551907200 784475   10805    Eco-devices TH..           120   DESKTOP-8D8NI3U
1571065552266123400 784475   10806    Eco-devices TH..           130   DESKTOP-8D8NI3U
1571065552349242000 784475   10806    Eco-devices TH..           130   DESKTOP-8D8NI3U
1571065552368053600 784475   10806    Eco-devices TH..           130   DESKTOP-8D8NI3U
1571065552464019700 784475   10806    Eco-devices TH..           130   DESKTOP-8D8NI3U
1571065552486374100 784475   10806    Eco-devices TH..           130   DESKTOP-8D8NI3U
1571065552567527100 784475   10806    Eco-devices TH..           140   DESKTOP-8D8NI3U
1571065552588459400 784475   10806    Eco-devices TH..           140   DESKTOP-8D8NI3U
```
#### Grafana

Voici ce que contient notre dashboard : 

![dashboard](https://i.imgur.com/tB4goB5.png)

En haut à droite du dashboard, on peut choisir un intervalle de temps. Toutes les visualisations vont ensuite se baser sur les données de cet intervalle. Les données des visualisations peuvent être mise à jour automatiquement avec la fréquence de notre choix.

Les visualisations : 
- Consommation cumulée 1 : affiche la consommation cumulée du sous-compteur 1
- Consommation cumulée 2 : affiche la consommation cumulée du sous-compteur 2
- Consommation actuelle : affiche la dernière valeur de consommation mesurée
- Moyenne par jour : affiche en barres la consommation moyenne par jour
- Evolution de la consommation : affiche toutes les valeurs de consommation sur la période donnée

### Plan de test
Pour tester notre installation, Romain démarrait sa passerelle et son serveur, Steevin démarrait la base de données, et Valentin démarrait Grafana. En rafraîchissement automatique, si Grafana affiche de nouvelles données toutes les 10 secondes, alors tout fonctionne comme attendu.
La passerelle transmet les données au serveur de base de données, qui sert ensuite de relais, soit pour écrire les données reçues soit pour les envoyer à Grafana pour qu'elles puissent être affichées dans le tableau de bord.


### Perspectives d'évolution du projet
Si nous devions faire évoluer ce projet à l'avenir, voici ce que nous y apporterions :
- Mesurer la consommation électrique sur un échantillon de données plus large pour obtenir des résultats plus pertinents
- Sécuriser MQTT en SSL
- Faire un test en réel afin de voir ce qui pourrait manquer à notre projet au niveau des fonctionnalités

### Bilans personnels

**Romain :**
Mon rôle sur ce projet était de réaliser la partie back. En effet, j'ai réalisé la passerelle qui se connecte au tableau électrique. Ensuite j'ai créé le lien entre la passerelle et le serveur. Le serveur que j'ai développé permet de récupérer les données envoyées depuis la passerelle et enfin de les envoyer vers la base de donnée InfluxDb utilisant le schéma mydb créé au préalable.
Ce projet m'a permis de découvrir le principe de MQTT ainsi que la base de données InfluxDb et enfin Grafana.
J'ai aussi créé un script run.sh qui permet de lancer la passerelle et le serveur en même temps.

**Steevin :**
Mon rôle sur ce projet a consisté à mettre en place la base de données InfluxDB, de la gérer et de m'assurer de son bon fonctionnement.
Ce projet m'a permis de découvrir une technologie de base de données différente de celles que j'ai pu utiliser jusqu'à aujourd'hui. Elle est intéressante dans le cas où l'on veut récupérer des mesures liées à des grandeurs spécifiques (consommation électrique, tension, température etc...). Son utilisation m'a semblé assez intuitive puisque sa logique est très proche de celle de MongoDB. Ma compréhension de la transmission de données entre différents modules est beaucoup plus claire maintenant.

**Valentin :** 
Sur ce projet, je me suis occupé de mettre en place Grafana. Je devais me connecter à la base de donnée InfluxDB hébergée par Steevin et créer des visualisations des données récupérées.
Ce projet m'a permis de découvrir et de manipuler un outil de monitoring simple à mettre en place, avec de larges possibilités. Je comprends aussi mieux ce qui se cache derrière mon propre compteur électrique.

### Conclusion
Pour conclure sur ce TP, nous avons pu découvrir une application de l'Internet des Objets concrète. On se rend compte qu'aujourd'hui, les objets de la vie quotidienne nous transforment un très grand nombre d'informations que l'on doit être capable de capturer et d'interpréter dans un temps raisonnable. La difficulté réside dans le choix d'étude des données. Il semble plus important de se focaliser sur certains paramètres qui vont nous amener à des conclusions pertinentes plutôt que d'autre comme nous l'a montré ce TP avec l'analyse de la consommation électrique. On n'a pas seulement appris à utiliser des nouveaux protocoles et technologies, mais aussi à prendre du recul sur la façon dont les objets nous transmettent des informations destinées à être traitées par nos soins.